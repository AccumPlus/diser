#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include <string>
#include <iostream>
#include <thread>

#include "QueueGetter.h"
#include "SafeQueue.h"
#include "Request.h"
#include "IndexTree.h"

QueueGetter::QueueGetter(Stopper &stopper):
	_stopper(stopper)
{

}

QueueGetter::~QueueGetter()
{

}

void QueueGetter::start()
{
	SafeQueue &safeQueue = SafeQueue::safeQueue();

	std::thread workingThread(&QueueGetter::working, this);

	while (true)
	{
		if (_stopper.isError() || _stopper.isStop())
		{
			safeQueue.resetAll();
			break;
		}
	}

	workingThread.join();
}

void QueueGetter::working()
{
	SafeQueue &safeQueue = SafeQueue::safeQueue();

	IndexTree &indexTree = IndexTree::indexTree();

	std::string message;

	while (true)
	{
		// TODO неблокирующая очередь
		safeQueue.wait();

		if (_stopper.isError() || _stopper.isStop())
		{
			break;
		}

		Request *request = safeQueue.pop();

		try
		{
			request->process();

			// Поиск в дереве
			std::vector<IndexTreeNode> points = indexTree.search(request->getPoint(), request->getCount(), request->getMaxDistance());

			// {"points":[{"id":[1,2,3]}, {"id2":[3,4,5]}]}
			message = "{\"points\":[ ";

			for (auto &point: points)
			{
				message += "{\"" + std::to_string(point.getId()) + "\":[ ";
				for (auto &dot: point.getPoint().getParams())
				{
					message += std::to_string(dot) + ',';
				}
				message.pop_back();
				message += "]},";
			}
			message.pop_back();
			message += "]}";
		}
		catch (AccumException &e)
		{
			message = std::string("{\"error\":\"") + e.what() + "\"}";
		}

		int clientSocket = request->getDescriptor();

		delete request;

		int bytesNumber = send(clientSocket, message.c_str(), message.size(), 0);

		if (bytesNumber < 0)
		{
			_stopper.setError(true);
			break;
		}
	}
}

