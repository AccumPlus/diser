#ifndef QUEUE_GETTER_H
#define QUEUE_GETTER_H

#include "Stopper.h"

class QueueGetter
{
	public:
		QueueGetter(Stopper &stopper);
		~QueueGetter();
		void start();
	private:
		void working();
		Stopper &_stopper;

};

#endif
