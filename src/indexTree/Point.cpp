#include <cmath>

#include "Point.h"

Point::Point(const std::vector<double> &params):
	_params(params)
{
}

Point::~Point()
{
}

Point operator - (const Point &p1, const Point &p2)
{
	std::vector<double> temp;
	auto it1 = p1._params.begin();
	auto it2 = p2._params.begin();
	
	while (it1 != p1._params.end() || it2 != p2._params.end())
	{
		if (it1 == p1._params.end())
		{
			temp.emplace_back(-*it2);
			++it2;
		}
		else if (it2 == p2._params.end())
		{
			temp.emplace_back(*it1);
			++it1;
		}
		else
		{
			temp.emplace_back(*it1 - *it2);
			++it1;
			++it2;
		}
	}

	return temp;
}

double Point::length() const
{
	double sum = 0;

	for (auto it: _params)
		sum += it * it;

	return pow(sum, 1/2.0);
}

double& Point::operator[](unsigned int index)
{
	return _params[index];
}

std::vector<double> Point::getParams() const
{
	return _params;
}

std::ostream& operator<<(std::ostream& os, const Point& p)
{
	std::string temp = "(";
	for (auto it: p._params)
		temp += std::to_string(it) + ',';
	if (temp.size() > 1)
		temp.pop_back();
	temp += ')';

	return os << temp;
}

std::string Point::toString() const
{
	std::string ret;
	for (auto it: _params)
		ret += std::to_string(it) + ',';
	if (ret.size() > 1)
		ret.pop_back();
	return ret;
}
