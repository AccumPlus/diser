#ifndef INDEX_TREE_H
#define INDEX_TREE_H


/* Класс IndexTree является самостоятельным модулем теневого хранения VP-дерева.
 * С этим модулем можно общаться по именованным каналам.
 */

#include <vector>
#include <mutex>
#include <condition_variable>

#include "vp-tree.h"
#include "Point.h"

class IndexTreeNode
{
	public:
		IndexTreeNode(const unsigned long &id, const Point &point);
		~IndexTreeNode();
//		friend IndexTreeNode operator - (const IndexTreeNode &p1, const IndexTreeNode &p2);
//		double length() const;
		unsigned long getId() const;
		Point getPoint() const;
//		std::vector<double> getParams() const;
	private:
		unsigned long _id;
		Point _point;
//		std::vector<double> _params;
};

double dist(const IndexTreeNode &p1, const IndexTreeNode &p2);

// Синглтон
class IndexTree
{
	public:
		static IndexTree &indexTree();
		~IndexTree();

		IndexTree(const IndexTree &) = delete;
		IndexTree& operator=(const IndexTree&) = delete;

		void updateTree(const std::vector<IndexTreeNode>&);
		std::vector<IndexTreeNode> search(const Point&, unsigned int count, double maxDistance);

	private:
		IndexTree();
		VpTree<IndexTreeNode, dist> _vpTree;

		std::mutex _cMutSearch;
		std::condition_variable _cVarSearch;
		unsigned short _isSearch;

		std::mutex _cMutUpdate;
		std::condition_variable _cVarUpdate;
		bool _isUpdate;

		std::mutex _mut;
};

#endif
