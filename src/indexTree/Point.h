#ifndef POINT_H
#define POINT_H

#include <vector>
#include <ostream>

class Point
{
	public:
		Point(const std::vector<double> &params);
		~Point();
		friend Point operator - (const Point &p1, const Point &p2);
		double length() const;
		std::vector<double> getParams() const;
		double& operator[](unsigned int index);
		friend std::ostream& operator<<(std::ostream& os, const Point& p); 
		std::string toString() const;
	private:
		std::vector<double> _params;
};

#endif
