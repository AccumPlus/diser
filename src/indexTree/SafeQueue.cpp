#include <iostream>

#include "SafeQueue.h"

SafeQueue& SafeQueue::safeQueue()
{
	static SafeQueue s;
	return s;
}

SafeQueue::SafeQueue(): _fullSize(0), _ready(false)
{
}

SafeQueue::~SafeQueue()
{
}

void SafeQueue::push(Request *request)
{
	std::lock_guard<std::mutex> lg(_mutex);
	_queue.push(request);

	std::unique_lock<std::mutex> ul(_cMutex);
	_ready = true;
	_cVar.notify_one();
}

Request* SafeQueue::pop()
{
	std::lock_guard<std::mutex> lg(_mutex);
	Request *tmp = _queue.front();
	_queue.pop();
	return tmp;
}

unsigned short SafeQueue::getSize()
{
	std::lock_guard<std::mutex> lg(_mutex);
	return _queue.size();
}

void SafeQueue::wait()
{
	{
		std::lock_guard<std::mutex> lg(_mutex);
		if (_queue.size() == 0)
			_ready = false;
	}

	std::unique_lock<std::mutex> ul(_cMutex);
	while (!_ready)
		_cVar.wait(ul);
}

void SafeQueue::resetAll()
{
	std::unique_lock<std::mutex> ul(_cMutex);
	_ready = true;
	_cVar.notify_all();
}
