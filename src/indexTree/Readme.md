Выражаю свои благодарности gregorburger за проект vp-tree и nlohmann за проект json, исходные файлы которых использовались при разработке модуля текущей директории.

Проекты взяты с сайтов https://github.com/gregorburger/vp-tree и https://github.com/nlohmann/json соответственно

Клонировать по ссылке: https://github.com/gregorburger/vp-tree.git и https://github.com/nlohmann/json.git
