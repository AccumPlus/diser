#include <iostream>
#include <thread>

#include <unistd.h>
#include <signal.h>
#include <sqlite3.h>

#include "QueueFiller.h"
#include "QueueGetter.h"
#include "Stopper.h"
#include "../accumexception/accumexception.h"
#include "IndexTree.h"
#include "../SQLiteCpp/SQLiteCpp.h"
#include "Table.h"
#include "../alglib/linalg.h"

Stopper stopper;

void catchSig(int s);

void updateTree(Stopper &stopper);

// TODO добавить обработку сигналов

int main()
{
	std::cout << "Hello, World!" << std::endl;

	struct sigaction sa;
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = catchSig;
	sigaction(SIGTERM, &sa, 0);
	sigaction(SIGINT, &sa, 0);

	std::thread fillerThread;

	QueueFiller *qFiller;

	try
	{
		qFiller = new QueueFiller{"./socket", 3, stopper};

		fillerThread = std::thread{&QueueFiller::reading, qFiller};
	}
	catch(AccumException &e)
	{
		std::cout << e.what() << std::endl;
		return -1;
	}

	QueueGetter qGetter(stopper);

	std::thread getterThread{&QueueGetter::start, qGetter};

	std::thread updateThread{updateTree, std::ref(stopper)};

	fillerThread.join();
	getterThread.join();
	updateThread.join();

	delete qFiller;

	return 0;
}

void updateTree(Stopper &stopper)
{
	SQLite::Database db{"../etc/example.db3", SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE};
	unsigned short timer = 0;

	while (true)
	{
		std::cout << "Update!" << std::endl;
		std::vector<IndexTreeNode> nodes;

		SQLite::Statement stWords(db, "select id from words");
		SQLite::Statement stFileWords(db, "select file_id, wordsCount from filewords where word_id = ?");

		Table table;
		while (stWords.executeStep())
		{
			int wordId = stWords.getColumn(0);
			stFileWords.reset();
			stFileWords.bind(1, wordId);

			std::map<unsigned long, int> values;
			unsigned long temp = 0;
			while (stFileWords.executeStep())
			{
				values[static_cast<unsigned long>((int)stFileWords.getColumn(0))] = stFileWords.getColumn(1);
				++temp;
			}

			// TODO проверить без удаления пустых
			if (temp > 1)
				table.addRow(wordId, values);
		}

		alglib::real_2d_array matrix(table.toString().c_str());

		alglib::real_1d_array w;
		alglib::real_2d_array u, vt;

		std::vector<unsigned long> verHeader = table.getVerHeader();
		std::vector<unsigned long> horHeader = table.getHorHeader();

		// U * W * VT
		// U - координаты слов
		// VT - координаты документов
		alglib::rmatrixsvd(matrix, verHeader.size(), horHeader.size(), 2, 2, 2, w, u, vt);

		auto wordsCoords = u.toPointVector(verHeader.size(), 2);

		alglib::real_2d_array v;
		v.setlength(vt.cols(), vt.rows());
		alglib::rmatrixtranspose(vt.rows(), vt.cols(), vt, 0, 0, v, 0, 0);

		auto docsCoords = v.toPointVector(horHeader.size(), 2);

		// Координаты слов просто через запятую
		SQLite::Statement stUpdWords(db, "update words set coords = ? where id = ?");
		auto headIt = verHeader.begin();
		auto coordsIt = wordsCoords.begin();

		std::cout << "Words" << std::endl;
		for (; headIt != verHeader.end() && coordsIt != wordsCoords.end(); ++headIt, ++coordsIt)
		{
			std::cout << (int)*headIt << ' ' << coordsIt->toString() << std::endl;

			stUpdWords.reset();
			stUpdWords.bind(1, coordsIt->toString());
			stUpdWords.bind(2, (int)*headIt);
			stUpdWords.exec();
		}

		std::cout << "Docs" << std::endl;
		// Координаты документов добавляем в дерево
		headIt = horHeader.begin();
		coordsIt = docsCoords.begin();
		for (; headIt != horHeader.end() && coordsIt != docsCoords.end(); ++headIt, ++coordsIt)
		{
			std::cout << *headIt << ' ' << coordsIt->toString() << std::endl;
			nodes.push_back(IndexTreeNode{*headIt, Point{*coordsIt}});
		}

//		nodes.push_back(IndexTreeNode(1, {2,19}));
//		nodes.push_back(IndexTreeNode(2, {6,19}));
//		nodes.push_back(IndexTreeNode(3, {7,18}));
//		nodes.push_back(IndexTreeNode(4, {3,17}));
//		nodes.push_back(IndexTreeNode(5, {5,17}));
//		nodes.push_back(IndexTreeNode(6, {15,17}));
//		nodes.push_back(IndexTreeNode(7, {17,16}));
//		nodes.push_back(IndexTreeNode(8, {19,16}));
//		nodes.push_back(IndexTreeNode(9, {5,15}));
//		nodes.push_back(IndexTreeNode(10, {20,15}));
//		nodes.push_back(IndexTreeNode(11, {19,14}));
//		nodes.push_back(IndexTreeNode(12, {10,13}));
//		nodes.push_back(IndexTreeNode(13, {16,13}));
//		nodes.push_back(IndexTreeNode(14, {12,10}));
//		nodes.push_back(IndexTreeNode(15, {4,8}));
//		nodes.push_back(IndexTreeNode(16, {8,8}));
//		nodes.push_back(IndexTreeNode(17, {3,7}));
//		nodes.push_back(IndexTreeNode(18, {5,6}));
//		nodes.push_back(IndexTreeNode(19, {7,6}));
//		nodes.push_back(IndexTreeNode(20, {20,6}));

		IndexTree &iTree = IndexTree::indexTree();

		iTree.updateTree(nodes);

		timer = 0;

		while (timer != 60)
		{
			std::cout << "2 secs" << std::endl;
			sleep(2);
			timer+=2;
			if (stopper.isStop() || stopper.isError())
				return;
		}
	}
}

void catchSig(int s)
{
	stopper.setStop(true);
}
