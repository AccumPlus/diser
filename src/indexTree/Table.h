#ifndef TABLE_H
#define TABLE_H

#include <vector>
#include <map>
#include <string>

class Table
{
	public:
		Table();
		Table(std::string);
		~Table();

		void addRow(const unsigned long &verHeader, const std::map<unsigned long, int> &values);
		std::string toString();
		//void excludeZeroRows();
		std::vector<unsigned long> getVerHeader();
		std::vector<unsigned long> getHorHeader();
	private:
		std::vector<unsigned long> _verHeader, _horHeader;
		std::map<long, std::map<unsigned long, int> > _table;
};

#endif
