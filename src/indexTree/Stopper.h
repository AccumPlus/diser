#ifndef STOPPER_H
#define STOPPER_H

#include <mutex>

class Stopper
{
	// Сделать множество ошибок
	public:
		Stopper();
		~Stopper();
		void setStop(const bool& stop);
		void setError(const bool& error);
		bool isStop();
		bool isError();

	private:
		bool _stop, _error;
		std::mutex mut;

};

#endif
