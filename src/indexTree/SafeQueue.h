#ifndef SAFE_QUEUE_H
#define SAFE_QUEUE_H

#include <queue>
#include <mutex>
#include <condition_variable>

#include "Request.h"

class SafeQueue
{
	public:
		static SafeQueue& safeQueue();
		~SafeQueue();

		void push(Request*);
		Request* pop();
		unsigned short getSize();
		void wait();
		void resetAll();
		SafeQueue(const SafeQueue&) = delete;
		SafeQueue& operator=(const SafeQueue&) = delete;

	private:
		SafeQueue();

		unsigned short _fullSize;
		std::queue<Request*> _queue;
		std::mutex _mutex;

		std::condition_variable _cVar;
		std::mutex _cMutex;
		bool _ready;
};

#endif
