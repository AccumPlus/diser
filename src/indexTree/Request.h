#ifndef REQUEST_H
#define REQUEST_H

#include <string>
#include <vector>

#include "Point.h"
#include "../accumexception/accumexception.h"

class Request
{
	public:

		Request(int descriptor, const std::string &text) throw (AccumException);
		~Request();

		void process() throw (AccumException);

		int getDescriptor();
		unsigned long getCount();
		double getMaxDistance();
		Point getPoint();

	private:
		int _descriptor;
		Point _point;
		unsigned long _count;
		double _maxDistance;
		std::string _rawData;
};

#endif
