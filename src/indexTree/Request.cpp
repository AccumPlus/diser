#include <regex>
#include <algorithm>
#include <cctype>

#include "Request.h"
#include "json.hpp"

Request::Request(int descriptor, const std::string &text) throw (AccumException):
	 _descriptor(descriptor), _point{{0}}, _count(0), _maxDistance(0.0), _rawData(text)
{
}

Request::~Request()
{
}

void Request::process() throw (AccumException)
{
	nlohmann::json _request = nlohmann::json::parse(_rawData);

	if (_request["point"].is_null())
	{
		throw AccumException(AccumException::INV_JSON_EXC);
	}
	else
	{
		std::vector<double> points;
		for (auto point: _request["point"])
		{
			points.push_back(point);
		}

		_point = points;
	}

	if (!_request["count"].is_null())
	{
		_count = _request["count"];
	}

	if (!_request["maxDistance"].is_null())
	{
		_maxDistance = _request["maxDistance"];
	}
}

int Request::getDescriptor()
{
	return _descriptor;
}

unsigned long Request::getCount()
{
	return _count;
}

double Request::getMaxDistance()
{
	return _maxDistance;
}

Point Request::getPoint()
{
	return _point;
}
