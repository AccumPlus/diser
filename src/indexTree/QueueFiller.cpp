#include <vector>
#include <thread>
#include <iostream>

#include <unistd.h>
#include <sys/socket.h>
#include <sys/un.h>

#include "QueueFiller.h"
#include "Request.h"
#include "SafeQueue.h"

QueueFiller::QueueFiller(const std::string &filename, const unsigned short &clientsNum, Stopper &stopper) throw (AccumException):
	_filename(filename), _clientsNum(clientsNum), _stopper(stopper), _isOpened(false)
{
	std::cout << "Openning socket... ";
	_serverSocket = socket(PF_LOCAL, SOCK_STREAM, 0);

	if (_serverSocket < 0)
	{
		std::cout << "Error!" << std::endl;
		throw AccumException(AccumException::SOCK_OPEN);
	}

	std::cout << "Done!" << std::endl;

	int reuseAddr = 1;
	setsockopt(_serverSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&reuseAddr, sizeof(reuseAddr));

	sockaddr_un srvAddr;

	srvAddr.sun_family = AF_LOCAL;
	strncpy(srvAddr.sun_path, _filename.c_str(), filename.length() + 1);

	size_t size = SUN_LEN(&srvAddr);

	std::cout << "Binding server... ";

	if (bind(_serverSocket, (struct sockaddr *) &srvAddr, size) < 0)
	{
		std::cout << "Error!" << std::endl;
		close(_serverSocket);
		std::remove(_filename.c_str());
		throw AccumException(AccumException::SOCK_BIND);
	}

	std::cout << "Done!" << std::endl;

	std::cout << "Listening server... ";

	if (listen (_serverSocket, _clientsNum) < 0)
	{
		std::cout << "Error!" << std::endl;
		close(_serverSocket);
		std::remove(_filename.c_str());
		throw AccumException(AccumException::SOCK_LISTEN);
	}

	std::cout << "Done!" << std::endl;

	_isOpened = true;
}

QueueFiller::~QueueFiller()
{
	if (_isOpened)
	{
		if (_serverSocket > 0)
		{
			close(_serverSocket);
			std::remove(_filename.c_str());
		}
	}
}

void QueueFiller::reading()
{
	fd_set readfds;

	int maxSocket = 0;
	int activity = 0;
	char *message = new char[1024];
	int bytesNumber = 0;
	std::vector<int> clientSockets(_clientsNum, 0);
	int clientSocket = 0;

	SafeQueue &safeQueue = SafeQueue::safeQueue();

	std::cout << "Start reading" << std::endl;

	while(true)
	{

		activity = 0;
		while (activity == 0)
		{
			if (_stopper.isError() || _stopper.isStop())
			{
				break;
			}

			struct timeval timeout;
			timeout.tv_sec = 2;
			timeout.tv_usec = 0;

			FD_ZERO(&readfds);

			FD_SET(_serverSocket, &readfds);

			maxSocket = _serverSocket;

			for (auto &sock: clientSockets)
			{
				if (sock > 0)
				{
					FD_SET(sock, &readfds);
					if (sock > maxSocket)
					{
						maxSocket = sock;
					}
				}

			}
			activity = select(maxSocket + 1, &readfds, NULL, NULL, &timeout);
		}

		if (_stopper.isError() || _stopper.isStop())
		{
			break;
		}

		if (activity < 0)
		{
			_stopper.setError(true);
			break;
		}
		else
		{
			// Incoming connection
			if (FD_ISSET(_serverSocket, &readfds))
			{
				std::cout << "Incoming connection" << std::endl;
				for (auto &sock: clientSockets)
				{
					if (sock == 0)
					{
						sock = accept(_serverSocket, 0, 0);
						std::cout << "Client socket = " << sock << std::endl;
						if (sock < 0)
						{
							sock = 0;
						}
						else
						{
							clientSocket = sock;
						}
						break;
					}
				}
			}

			// Changes on clients
			for (auto &sock: clientSockets)
			{
				if (FD_ISSET(sock, &readfds))
				{
					std::cout << "Change on client: " << sock << std::endl;
					message[0] = '\0';
					bytesNumber = 0;
					bytesNumber = recv(sock, message, 1023, 0);

					if (bytesNumber <= 0)
					{
						std::cout << "Disconnect!" << std::endl;
						close(sock);
						sock = 0;
					}
					else
					{
						message[bytesNumber] = '\0';
						std::cout << "Message = " << message << std::endl;
						Request *request = new Request(sock, message);
						safeQueue.push(request);
					}
				}
			}
		}

	}

	delete[] message;

	for (auto &sock: clientSockets)
	{
		if (sock > 0)
		{
			close(sock);
		}
	}

	if (_isOpened)
	{
		if (_serverSocket > 0)
		{
			close(_serverSocket);
			std::remove(_filename.c_str());
			_serverSocket = 0;
		}
		_isOpened = false;
	}
}
