#include "Stopper.h"

Stopper::Stopper()
{
	std::lock_guard<std::mutex> lg(mut);
	_stop = false;
	_error = false;
}

Stopper::~Stopper()
{

}

void Stopper::setStop(const bool& stop)
{
	std::lock_guard<std::mutex> lg(mut);
	_stop = stop;
}

void Stopper::setError(const bool& error)
{
	std::lock_guard<std::mutex> lg(mut);
	_error = error;
}

bool Stopper::isStop()
{
	std::lock_guard<std::mutex> lg(mut);
	return _stop;
}

bool Stopper::isError()
{
	std::lock_guard<std::mutex> lg(mut);
	return _error;
}
