#include <algorithm>
#include <iostream>

#include "Table.h"

Table::Table()
{
}

Table::Table(std::string raw)
{

}

Table::~Table()
{
}

void Table::addRow(const unsigned long &verHeader, const std::map<unsigned long, int> &values)
{
	if (std::find(_verHeader.begin(), _verHeader.end(), verHeader) != _verHeader.end())
	{
		std::cout << "Row with given header is already exists!" << std::endl;
		return;
	}

	// Добавление вертикального хедера
	_verHeader.push_back(verHeader);

	// Добавление недостающих элементов горизонтального хедера
	for (auto elem: values)
		if (std::find(_horHeader.begin(), _horHeader.end(), elem.first) == _horHeader.end())
			_horHeader.push_back(elem.first);

	// Добавление значений
	_table[verHeader] = values;
}

std::string Table::toString()
{
	std::string ret = "[";

	for (auto vEl: _verHeader)
	{
		ret += "[";
		for (auto hEl: _horHeader)
			ret += std::to_string(_table[vEl][hEl]) + ',';
		ret.pop_back();
		ret += "],";
	}
	ret.pop_back();

	ret += "]";

	return ret;
}

//void Table::excludeZeroRows()
//{
//	for (auto it = _verHeader.begin(); it != _verHeader.end();)
//	{
//		unsigned long zeros = 0;
//		for (auto hEl: _horHeader)
//		{
//			if (_table[*it][hEl] == 0)
//				++zeros;
//		}
//
//		if (zeros >= _horHeader.size() - 1)
//		{
//			_table.erase(*it);
//			it =_verHeader.erase(it);
//		}
//		else
//			++it;
//	}
//}

std::vector<unsigned long> Table::getVerHeader()
{
	return _verHeader;
}

std::vector<unsigned long> Table::getHorHeader()
{
	return _horHeader;
}
