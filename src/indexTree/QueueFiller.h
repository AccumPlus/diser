#ifndef QUEUE_FILLER_H
#define QUEUE_FILLER_H

#include <string>

#include "Stopper.h"
#include "../accumexception/accumexception.h"

class QueueFiller
{
	public:
		QueueFiller(const std::string &filename, const unsigned short &clientsNum, Stopper &stopper) throw (AccumException);
		~QueueFiller(); 
		void reading();
	private:
		std::string _filename;
		unsigned short _clientsNum;
		int _serverSocket;
		Stopper &_stopper;
		bool _isOpened;
};

#endif
