#include <cmath>

#include <unistd.h>

#include "IndexTree.h"

IndexTree::IndexTree():
	_isSearch{0}, _isUpdate{false}
{
}

IndexTree &IndexTree::indexTree()
{
	static IndexTree tree;
	return tree;
}

IndexTree::~IndexTree()
{
}

void IndexTree::updateTree(const std::vector<IndexTreeNode>&points)
{
	// Блокировка поиска
	{
		std::lock_guard<std::mutex> lg(_mut);
		_isUpdate = true;
	}
	
	// Ожидание завершения поиска
	std::unique_lock<std::mutex> ul(_cMutUpdate);
	while (_isSearch != 0)
		_cVarUpdate.wait(ul);
	
	// Обновление
	_vpTree.create(points);

	// Разрешение поиска
	std::lock_guard<std::mutex> lg(_mut);
	_isUpdate = false;
	_cVarSearch.notify_all();
}

std::vector<IndexTreeNode> IndexTree::search(const Point &point, unsigned int count, double maxDistance)
{
	// Ожидание завершения обновления
	std::unique_lock<std::mutex> ul(_cMutSearch);
	while (_isUpdate)
		_cVarSearch.wait(ul);

	// Блокировка обновления
	{
		std::lock_guard<std::mutex> lg(_mut);
		++_isSearch;
	}
	
	// Поиск
	std::vector<IndexTreeNode> neighbours;
	std::vector<double> distances;
	_vpTree.search(IndexTreeNode{0, point}, count, &neighbours, &distances);
	auto it = distances.begin();
	auto jt = neighbours.begin();
	for (; jt != neighbours.end(); ++jt, ++it)
		if (*it > maxDistance)
			break;
	
	// Разрешение обновления
	std::lock_guard<std::mutex> lg(_mut);
	if (--_isSearch == 0)
	{
		_cVarUpdate.notify_all();
	}

	return std::vector<IndexTreeNode>(neighbours.begin(), jt);
}

double dist(const IndexTreeNode &p1, const IndexTreeNode &p2)
{
	return (p1.getPoint() - p2.getPoint()).length();
}

IndexTreeNode::IndexTreeNode(const unsigned long &id, const Point &point):
	_id(id), _point(point)
{
}

IndexTreeNode::~IndexTreeNode()
{
}

unsigned long IndexTreeNode::getId() const
{
	return _id;
}

Point IndexTreeNode::getPoint() const
{
	return _point;
}
