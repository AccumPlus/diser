#ifndef STORAGE_H
#define STORAGE_H

#include <vector>
#include <string>
#include <map>

namespace SQLite
{
	class Database;
}

class Storage
{
	public:
		Storage();
		~Storage();

		void addFile(const std::string &filePath);
		void remFile(const std::string &fileName);
//		void update();
		

	//	std::map<int, std::string> searchFile(const std::string &searchText);
	
	private:

		static const std::vector<std::string> stopWords;
		SQLite::Database *db;

		


};

#endif
