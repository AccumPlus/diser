#ifndef ACCUMEXCEPTION_H
#define ACCUMEXCEPTION_H

#include <exception>
#include <string>


class AccumException: public std::exception
{
	public:
		enum AccumExc:int{	UNDEFINED				= 0,
							NO_ERR					= 1,
							DEFAULT_EXC				= 100,
							INV_JSON_EXC			= 101,
							SOCK_OPEN				= 200,
							SOCK_BIND				= 201,
							SOCK_LISTEN				= 202
		};
		AccumException(AccumExc exc);
		virtual const char* what() const noexcept;
		AccumExc getCode() const;
		~AccumException();
	private:
		std::string getExcMessage(AccumExc exc) const;
		std::string error;
		AccumExc code;
};

#endif
