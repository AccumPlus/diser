#include <errno.h>

#include "accumexception.h"

AccumException::AccumException(AccumExc exc):
	error(getExcMessage(exc)), code(exc)
{
}

const char* AccumException::what() const noexcept
{
	return error.c_str();
}

std::string AccumException::getExcMessage(AccumExc exc) const
{
	std::string message("***ACCUMEXCEPTION***: ");
	switch (exc)
	{
		case NO_ERR:
			message += "No error.";
			break;
		case DEFAULT_EXC:
			message += "Other exception.";
			break;
		case INV_JSON_EXC:
			message += "Bad json file.";
			break;
		default:
			break;
	}

	if (exc == DEFAULT_EXC)
		message += " Error code = " + std::to_string(errno);
	else
		message += " Exception code = " + std::to_string(exc);


	return message;
}

AccumException::~AccumException()
{
}

AccumException::AccumExc AccumException::getCode() const
{
	return code;
}
