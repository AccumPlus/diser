#include <cstdio>
#include <string>
#include <array>
#include <memory>
#include <iostream>
#include <regex>
#include <algorithm>
#include <map>

#include "SQLiteCpp/SQLiteCpp.h"
#include <sqlite3.h>

#include "Storage.h"
//#include "Table.h"
//#include "alglib/linalg.h"

const std::vector<std::string> Storage::stopWords = {
"еще",		"сказать",	"а",		"ж",		"со",
"без",		"же",		"совсем",	"более",	//"жизнь",
"нельзя",	"так",		"больше",	"за",		"нет",
"такой",	"будет",	"зачем",	"ни",		"там",
"будто",	"здесь",	"нибудь",	"бы",		"и",
"никогда",	"был",		"из",		"теперь",	"из-за",
"них",		"то",		"или",		"ничего",	"тогда",
"но",		"быть",		"иногда",	"ну",		"тоже",
"в",		"о",		"только",	"к",		"об",
"вас",		"кажется",	"один",		"тот",		"вдруг",
"как",		"он",		"три",		"ведь",		"она",
"тут",		"во",		"какой",	"они",		"ты",
"вот",		"когда",	"опять",	"у",		"впрочем",
"конечно",	"от",		"уж",		"все",		"перед",
"уже",		"всегда",	"по",		"хорошо",	"всего",
"кто",		"под",		"хоть",		"куда",		"после",
"чего",		"ли",		"потом",	"человек",	"вы",
"лучше",	"потому",	"чем",		"г",		"между",
"почти",	"через",	"где",		"при",		"что",
"мне",		"про",		"чтоб",		"да",		"много",
"раз",		"чтобы",	"даже",		"может",	"разве",
"чуть",		"два",		"можно",	"с",		"эти",
"для",		"мой",		"сам",		"до",		"другой",
"мы",		"на",		"этот",		"над",		"себя",
"надо",		"сегодня",	"я",		"наконец",	"сейчас",
"если",		"не",		"который",	"весь",		"свой",
"это",		"становиться"};

Storage::Storage()
{
	try
	{
		db = new SQLite::Database("../etc/example.db3", SQLITE_OPEN_READWRITE|SQLITE_OPEN_CREATE);

		SQLite::Transaction tr(*db);

		db->exec("create table if not exists Files (id integer primary key autoincrement not null, filename varchar(128))");
		db->exec("create table if not exists Words (id integer primary key autoincrement not null, word varchar(128), coords varchar(128), unique(word))");
		db->exec("create table if not exists FileWords (id integer primary key autoincrement not null, file_id integer not null, word_id integer not null, wordsCount integer not null, wordsFreq real not null)");
		db->exec("create index if not exists index_FileWords on Words (word)");

		tr.commit();
		
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
}

Storage::~Storage()
{
	delete db;
}

void Storage::addFile(const std::string &filename)
{
	std::string result;
	std::shared_ptr<FILE> pipe(popen((std::string("../additional/mystem -n ") + filename).c_str(), "r"), pclose);
	if (!pipe) throw std::runtime_error("popen() failed!");

	std::regex re("^.*\\{([^|?}]+)(\\?*).*\\}$");
	std::smatch match;
	std::map<std::string, unsigned long long> dict;
	std::string line;
	unsigned long long wordsCnt = 0;

	while (!feof(pipe.get()))
	{
		char c = fgetc(pipe.get());
		if (c == '\n' && !line.empty())
		{
			if (std::regex_search(line, match, re))
			{
				if (std::string(match[2]).length() < 2)
				{
					line = match[1];
					if (std::find(stopWords.begin(), stopWords.end(), line) == stopWords.end())
					{
						dict[line]++;
						wordsCnt++;
					}
				}
			}

			line.clear();
		}
		else
		{
			line += c;
		}
	}


	SQLite::Transaction tr(*db);
	try
	{
		std::stringstream query;
		query << "insert into Files (filename) values (\"" << filename << "\")";
		db->exec(query.str());

		SQLite::Statement stFiles(*db, "select last_insert_rowid() from Files");

		stFiles.executeStep();
		int fileId = stFiles.getColumn(0);

		SQLite::Statement stWords(*db, "select last_insert_rowid() from Words");
		SQLite::Statement stSelWord(*db, "select id from Words where word = ?");

		for (auto word: dict)
		{
			int wordId = 0;

			stSelWord.reset();
			stSelWord.bind(1, word.first);
			if (stSelWord.executeStep())
			{
				wordId = stSelWord.getColumn(0);
			}
			else
			{
				query.str("");
				query << "insert into Words (word) values (\"" << word.first << "\")";
				db->exec(query.str());

				stWords.reset();
				stWords.executeStep();
				wordId = stWords.getColumn(0);
			}

			query.str("");
			query << "insert into FileWords (file_id, word_id, wordsCount, wordsFreq) values (" << fileId << ", " << wordId << ", " << word.second << ", " << word.second / (double)wordsCnt * 100 << ")";
			db->exec(query.str());
		}
	}
	catch (std::exception &e)
	{
		std::cout << "Exception: " << e.what() << std::endl;
	}
	tr.commit();
}

//void Storage::update()
//{
//	SQLite::Statement stWords(*db, "select id from words");
//	SQLite::Statement stFileWords(*db, "select file_id, wordsCount from filewords where word_id = ?");
//
//	Table table;
//	while (stWords.executeStep())
//	{
//		int wordId = stWords.getColumn(0);
//		stFileWords.reset();
//		stFileWords.bind(1, wordId);
//
//		std::map<int, int> values;
//		unsigned long temp = 0;
//		while (stFileWords.executeStep())
//		{
//			values[stFileWords.getColumn(0)] = stFileWords.getColumn(1);
//			++temp;
//		}
//
//		// TODO проверить без удаления пустых
//		if (temp > 1)
//			table.addRow(wordId, values);
//	}
//
//	alglib::real_2d_array matrix(table.toString().c_str());
//
//	alglib::real_1d_array w;
//	alglib::real_2d_array u, vt;
//
//	std::vector<int> verHeader = table.getVerHeader();
//	std::vector<int> horHeader = table.getHorHeader();
//
//	// U * W * VT
//	// U - координаты слов
//	// VT - координаты документов
//	alglib::rmatrixsvd(matrix, verHeader.size(), horHeader.size(), 2, 2, 2, w, u, vt);
//
//	auto wordsCoords = u.toPointVector(verHeader.size(), 2);
//
//	alglib::real_2d_array v;
//	v.setlength(vt.cols(), vt.rows());
//	alglib::rmatrixtranspose(vt.rows(), vt.cols(), vt, 0, 0, v, 0, 0);
//
//	auto docsCoords = v.toPointVector(horHeader.size(), 2);
//
//	// Координаты слов просто через запятую в БД
//	SQLite::Statement stUpdWords(*db, "update words set coords = ? where id = ?");
//	auto headIt = verHeader.begin();
//	auto coordsIt = wordsCoords.begin();
//
//	for (; headIt != verHeader.end() && coordsIt != wordsCoords.end(); ++headIt, ++ coordsIt)
//	{
//		stUpdWords.reset();
//		stUpdWords.bind(1, coordsIt->toString());
//		stUpdWords.bind(2, *headIt);
//		stUpdWords.exec();
//	}

	// Координаты документов добавляем в дерево
	

//	std::cout << u.tostring(2) << std::endl;
//
//	std::cout << "words" << std::endl;
//	for (unsigned int i = 0; i < verHeader.size(); ++i)
//		std::cout << verHeader[i] << ' ' << wordsCoords[i] << std::endl;
//
//	std::cout << "docs" << std::endl;
//	for (auto jt: horHeader)
//		std::cout << jt << ' ';
//	std::cout << std::endl;
//	for (auto i: docsCoords)
//		std::cout << i << std::endl;
	// Надо отсечь ненужные строки. Для прототипа будем строить в 2D
	

	// В поисковом запросе приходит слово.
	// В БД ищется это слово и берутся его координаты
	// В VP-дереве ищутся соседи.
	// ?????????
	// Профит.
	// То есть достаточно сохранить координаты слов в БД, а координаты документов в дереве

	// Сохраняем в БД
//	SQLite::Statement stUpdWords(*db, "update words set x = ?, y = ? where id = ?");
//	for (auto it: verHeader)
//	{
//		stUpdWords.reset();
//		stUpdWords.bind(1, )
//
//	}

	




	// строим матрицу документ-слово. На пересечении - параметр слова (частота, например)
	// Матрицу нужно правильно обработать. Шансов мало, но могут быть документы, у которых
	// ни одно слово не встретится в другом документе. Что делать в таком случае - хз. Но скорее
	// всего ничего делать не нужно, чтобы качество анализа было выше.
	
	// делаем сингулярное разложение. Отбрасываем строки и столбцы, чтобы размерность была 2.
	
	// добавляем две таблицы - точки файлов и точки слов. На этом всё, дальше дело поиска.
	
//}

//std::map<int, std::string> Storage::searchFile(const std::string &searchText)
//{
//	// Бьём на слова
//	// Ищем слово
//	// Ищем документы, близкие к слову по растоянию
//	
//
//}
