#include <exception>
#include <iostream>

#include <unistd.h>

#include "Storage.h"

int main()
{
	Storage storage;

	// Отдельный процесс для обновления данных
	pid_t pid;

	pid = fork();
	if (pid == -1) // Error
	{
		return -1;
	}
	else if (pid == 0) // Child
	{
		// Индексатор
		execl("lib/IndexTree", "lib/IndexTree", nullptr);
	}
	else // Parent
	{
		// Родитель читает запросы от пользователя и выполняет необходимые манипуляции

		// Предполагаем, что сервер настроен и из запросов выделены необходимые данные
		
		// У запроса будет определяться тип. Пока что будет грубый пример
		
		unsigned short type = 0;

		switch (type)
		{
			case 0:		// Запрос на поиск
			{
				// Подключаемся к файловому сокету индексатора
				// Отправляем запрос
				// Ждём ответ, ну и выводим на экран
				break;
			}
			case 1:
			{

				break;
			}
			default:
			{

			}
		}

		
	}



//	storage.addFile("../additional/themes/text01.txt");
//	storage.addFile("../additional/themes/text02.txt");
//	storage.addFile("../additional/themes/text03.txt");
//	storage.addFile("../additional/themes/text04.txt");
//	storage.addFile("../additional/themes/text11.txt");
//	storage.addFile("../additional/themes/text12.txt");
//	storage.addFile("../additional/themes/text13.txt");
//	storage.addFile("../additional/themes/text21.txt");
//	storage.addFile("../additional/themes/text22.txt");
//	storage.addFile("../additional/themes/text23.txt");

//	storage.addFile("../additional/temp01.txt");
//	storage.addFile("../additional/temp02.txt");
//	storage.addFile("../additional/temp03.txt");
//	storage.addFile("../additional/temp04.txt");
//	storage.addFile("../additional/temp05.txt");
//	storage.addFile("../additional/temp06.txt");
//	storage.addFile("../additional/temp07.txt");
//	storage.addFile("../additional/temp08.txt");
//	storage.addFile("../additional/temp09.txt");

//	storage.addFile("../additional/dictants/short01.txt");
//	storage.addFile("../additional/dictants/short01.txt");
//	storage.addFile("../additional/dictants/short01.txt");
//	storage.addFile("../additional/dictants/short11.txt");
//	storage.addFile("../additional/dictants/short11.txt");
//	storage.addFile("../additional/dictants/short11.txt");
//	storage.addFile("../additional/dictants/short21.txt");
//	storage.addFile("../additional/dictants/short21.txt");
//	storage.addFile("../additional/dictants/short21.txt");
//	storage.addFile("../additional/dictants/short31.txt");
//	storage.addFile("../additional/dictants/short31.txt");
//	storage.addFile("../additional/dictants/short31.txt");
//	storage.update();
	
//	IndexTree indexTree;
//
//	std::vector<IndexTreeNode> nodes;
//
//	nodes.push_back(IndexTreeNode(1, {2,19}));
//	nodes.push_back(IndexTreeNode(2, {6,19}));
//	nodes.push_back(IndexTreeNode(3, {7,18}));
//	nodes.push_back(IndexTreeNode(4, {3,17}));
//	nodes.push_back(IndexTreeNode(5, {5,17}));
//	nodes.push_back(IndexTreeNode(6, {15,17}));
//	nodes.push_back(IndexTreeNode(7, {17,16}));
//	nodes.push_back(IndexTreeNode(8, {19,16}));
//	nodes.push_back(IndexTreeNode(9, {5,15}));
//	nodes.push_back(IndexTreeNode(10, {20,15}));
//	nodes.push_back(IndexTreeNode(11, {19,14}));
//	nodes.push_back(IndexTreeNode(12, {10,13}));
//	nodes.push_back(IndexTreeNode(13, {16,13}));
//	nodes.push_back(IndexTreeNode(14, {12,10}));
//	nodes.push_back(IndexTreeNode(15, {4,8}));
//	nodes.push_back(IndexTreeNode(16, {8,8}));
//	nodes.push_back(IndexTreeNode(17, {3,7}));
//	nodes.push_back(IndexTreeNode(18, {5,6}));
//	nodes.push_back(IndexTreeNode(19, {7,6}));
//    nodes.push_back(IndexTreeNode(20, {20,6}));
//
//	indexTree.updateTree(nodes);
//	auto found = indexTree.search({20,6}, 20, 10);
//
//	for (auto it: found)
//	{
//		std::cout << it.getId() << ": ";
//		for (auto jt: it.getParams())
//			std::cout << jt << ", ";
//		std::cout << std::endl;
//	}

	return 0;
}
